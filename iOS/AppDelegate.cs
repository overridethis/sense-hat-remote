﻿using Foundation;
using HatRemote.Networking.BluetoothLE;
using UIKit;

namespace HatRemote.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            BLEManager.Instance = nexus.protocols.ble.BluetoothLowEnergyAdapter.ObtainDefaultAdapter();

            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }
    }
}
