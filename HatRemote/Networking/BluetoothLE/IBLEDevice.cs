﻿using System;
using System.Threading.Tasks;

namespace HatRemote.Networking.BluetoothLE
{
    public interface IBLEDevice
    {
        bool IsDisconnected { get; }

        void Read<TData>(Guid characteristicId, Action<TData> callback);
        Task<TData> ReadAsync<TData>(Guid characteristicId);
        Task<string> ReadAsStringAsync(Guid characteristicId);
        Task WriteAsync<TData>(Guid characteristicId, TData payload);
    }
}