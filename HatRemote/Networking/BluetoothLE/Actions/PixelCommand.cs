﻿using System;
using System.Threading.Tasks;
using HatRemote.Models;
using HatRemote.Networking.Actions;

namespace HatRemote.Networking.BluetoothLE.Actions
{
    public class PixelCommand : BLECommand, IPixelCommand
    {
        public PixelCommand(Func<Task<IBLEDevice>> connect) : base(connect)
        {
        }

        public Task WriteAsync(LEDCell data) => DoWrite(BLEService.Characteristics.PixelGuid, data.ToPayload());
    }
}
