﻿using System;
using System.Threading.Tasks;
using HatRemote.Models;
using HatRemote.Networking.Actions;

namespace HatRemote.Networking.BluetoothLE.Actions
{
    public class IMUSensorsCommand : BLECommand, IIMUSensorsCommand
    {
        public IMUSensorsCommand(Func<Task<IBLEDevice>> connect) : base(connect)
        {
        }

        public void Read(Action<IMUSensors> callback) => DoRead(BLEService.Characteristics.IMUSensorsGuid, callback);
    }
}
