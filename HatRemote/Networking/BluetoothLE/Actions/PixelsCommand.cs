﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HatRemote.Models;
using HatRemote.Networking.Actions;

namespace HatRemote.Networking.BluetoothLE.Actions
{
    public class PixelsCommand : BLECommand, IPixelsCommand
    {
        public PixelsCommand(Func<Task<IBLEDevice>> connect) : base(connect)
        {
        }

        public void Read(Action<LEDMatrix> callback)
        {
            Task.Run(async () =>
            {
                // NOTE: Because the payload for BluetoothLE transmission cannot be larger than 
                // 512 bytes (approx.) we split the payload into two different characteristics that represent
                // 4 rows (8 total) in each load of data.
                var pixels1 = DoReadAsync<List<int[]>>(BLEService.Characteristics.Pixels1Guid);
                var pixels2 = DoReadAsync<List<int[]>>(BLEService.Characteristics.Pixels2Guid);

                await Task.WhenAll(pixels1, pixels2);

                var merged = pixels1.Result ?? new List<int[]>();
                merged.AddRange(pixels2.Result ?? new List<int[]>());
                callback(LEDMatrix.From(merged));
            });
        }
        public async Task WriteAsync(LEDMatrix data) {
            await DoWrite(BLEService.Characteristics.Pixels1Guid, data.Cells.Take(32));
            await DoWrite(BLEService.Characteristics.Pixels2Guid, data.Cells.Skip(32).Take(32));
        }
    }
}
