﻿using System;
using System.Threading.Tasks;

namespace HatRemote.Networking.BluetoothLE.Actions
{
    public abstract class BLECommand
    {
        private IBLEDevice _device;
        private readonly Func<Task<IBLEDevice>> _connect;

        protected BLECommand(
            Func<Task<IBLEDevice>> connect)
        {
            this._connect = connect;
        }

        protected async Task<IBLEDevice> ConnectAsync() {
            if (_device == null || _device.IsDisconnected)
                _device = await _connect();
            return _device;
        }

        protected void DoRead<TPayload>(Guid characteristicGuid, Action<TPayload> callback)
        {
            Task.Run(async () =>
            {
                var device = await ConnectAsync();
                device.Read<TPayload>(characteristicGuid, callback);
            });
        }

        protected async Task<string> DoReadAsStringAsync(Guid characteristicGuid)
        {
            var device = await ConnectAsync();
            var payload = await device.ReadAsStringAsync(characteristicGuid);
            return payload;
        }

        protected async Task<TPayload> DoReadAsync<TPayload>(Guid characteristicGuid)
        {
            var device = await ConnectAsync();
            var payload = await device.ReadAsync<TPayload>(characteristicGuid);
            return payload;
        }

        protected async Task DoWrite<TPayload>(Guid characteristicGuid, TPayload payload)
        {
            var device = await ConnectAsync();
            await device.WriteAsync<TPayload>(characteristicGuid, payload);
        }
    }
}
