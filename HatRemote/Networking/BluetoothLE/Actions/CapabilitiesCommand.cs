﻿using System;
using System.Threading.Tasks;
using HatRemote.Models;
using HatRemote.Networking.Actions;
using Newtonsoft.Json.Linq;

namespace HatRemote.Networking.BluetoothLE.Actions
{
    public class CapabilitiesCommand : BLECommand, ICapabilitiesCommand
    {
        public CapabilitiesCommand(
            Func<Task<IBLEDevice>> connect) : base(connect)
        {

        }
        
        public async Task<Capabilities> ReadAsync()
        {
            var raw = await DoReadAsStringAsync(BLEService.Characteristics.CapabilitiesGuid);

            var j = JObject.Parse(raw);
            var capabilities = Capabilities.Parse(j);

            return capabilities;
        }
    }
}
