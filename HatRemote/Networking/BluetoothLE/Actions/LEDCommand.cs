﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HatRemote.Models;
using HatRemote.Networking.Actions;

namespace HatRemote.Networking.BluetoothLE.Actions
{
    public class LEDCommand : BLECommand, ILEDCommand
    {
        public LEDCommand(Func<Task<IBLEDevice>> connect) : base(connect)
        {
        }

        public Task WriteAsync(LEDCommandArg data) => DoWrite(BLEService.Characteristics.LEDCommandGuid, data);
    }
}
