﻿using System;
using System.Threading.Tasks;
using HatRemote.Models;
using HatRemote.Networking.Actions;

namespace HatRemote.Networking.BluetoothLE.Actions
{

    public class EnviromentalSensorsCommand : BLECommand, IEnvironmentalSensorsCommand
    {
        public EnviromentalSensorsCommand(
            Func<Task<IBLEDevice>> connect) : base(connect)
        {
        }

        public void Read(Action<EnvironmentalSensors> callback) => DoRead(BLEService.Characteristics.EnvSensorsGuid, callback);
    }
}
