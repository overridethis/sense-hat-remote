﻿using System;

namespace HatRemote.Networking.BluetoothLE
{
    public class BLECharacteristics
    {
        public Guid CapabilitiesGuid => new Guid("5fd1c005-492c-4f33-87d3-a233ccf3c6c1");
        public Guid EnvSensorsGuid => new Guid("5fd1c005-492c-4f33-87d3-a233ccf3c6e1");
        public Guid IMUSensorsGuid => new Guid("5fd1c005-492c-4f33-87d3-a233ccf3c6e2");
        public Guid LEDCommandGuid => new Guid("5fd1c005-492c-4f33-87d3-a233ccf3c6e3");
        public Guid PixelGuid => new Guid("5fd1c005-492c-4f33-87d3-a233ccf3c6e4");

        // NOTE: This had to be split because of message size
        // limitations. 
        public Guid Pixels1Guid => new Guid("5fd1c005-492c-4f33-87d3-a233ccf3c6f0");
        public Guid Pixels2Guid => new Guid("5fd1c005-492c-4f33-87d3-a233ccf3c6f1");
    }
}

