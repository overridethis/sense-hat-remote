﻿using System;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using nexus.protocols.ble;

namespace HatRemote.Networking.BluetoothLE
{
    public class BLEDevice : IBLEDevice
    {
        private readonly IBleGattServerConnection _gattServer;

        public BLEDevice(IBleGattServerConnection gattServer)
        {
            this._gattServer = gattServer;
        }

        public bool IsDisconnected => _gattServer.State == ConnectionState.Disconnected || _gattServer.State == ConnectionState.Disconnecting;

        public void Read<TData>(
            Guid characteristicId,
            Action<TData> callback)
        {
            Task.Run(async () =>
            {
                var bytes = await this._gattServer
                    .ReadCharacteristicValue(BLEService.Guid, characteristicId);
                var j = Encoding.UTF8.GetString(bytes);
                var payload = JsonConvert.DeserializeObject<TData>(j);
                callback(payload);
            });
        }

        public async Task<string> ReadAsStringAsync(Guid characteristicId)
        {
            var bytes = await this._gattServer
                .ReadCharacteristicValue(BLEService.Guid, characteristicId);
            return Encoding.UTF8.GetString(bytes);
        }

        public async Task<TData> ReadAsync<TData>(Guid characteristicId)
        {
            var bytes = await this._gattServer
                .ReadCharacteristicValue(BLEService.Guid, characteristicId);
            var j = Encoding.UTF8.GetString(bytes);
            var payload = JsonConvert.DeserializeObject<TData>(j);
            return payload;
        }

        public async Task WriteAsync<TData>(
            Guid characteristicId,
            TData payload)
        {
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
            };

            var j = JsonConvert.SerializeObject(payload, settings);
            var bytes = Encoding.UTF8.GetBytes(j);
            await this._gattServer.WriteCharacteristicValue(
                BLEService.Guid,
                characteristicId,
                bytes);
        }
    }
}
