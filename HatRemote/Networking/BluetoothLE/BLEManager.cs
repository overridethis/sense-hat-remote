﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using HatRemote.Services;
using nexus.protocols.ble;
using nexus.protocols.ble.scan;

namespace HatRemote.Networking.BluetoothLE
{
    public static class BLEManager
    {
        private static IBluetoothLowEnergyAdapter _adapter;
        private static IConfiguration _config = Configuration.Instance;

        public static IBluetoothLowEnergyAdapter Instance
        {
            private get
            {
                if (_adapter == null)
                    throw new NullReferenceException("BLEManager - adapter not initialized.");
                return _adapter;
            }
            set
            {
                _adapter = value;
            }
        }

        public static void Scan(Action<string> callback)
        {
            var filter = new ScanFilter();
            filter.AddAdvertisedService(BLEService.Guid);

            var devices = new List<IBlePeripheral>();
            var scan = _adapter.ScanForBroadcasts(
                filter: filter,
                advertisementDiscovered: peripheral =>
                {
                    if (devices.Contains(peripheral))
                        return;
                    devices.Add(peripheral);
                    callback(peripheral.Advertisement.DeviceName);
                }, 
                timeout: TimeSpan.FromSeconds(10));
        }

        private static async Task<BLEDevice> ConnectAsync(string deviceName)
        {
            var filter = new ScanFilter();
            filter.SetAdvertisedDeviceName(deviceName);
            var connection = await _adapter.FindAndConnectToDevice(
                filter: filter,
                timeout: TimeSpan.FromSeconds(10));
            if (connection.IsSuccessful())
                return new BLEDevice(connection.GattServer);
            throw new Exception("[BLEManager] Unable to connect in (10s timeout).");
        }

        public static async Task<IBLEDevice> ConfiguredDevice()
        {
            return await ConnectAsync(_config.DeviceName);
        }
    }
}
