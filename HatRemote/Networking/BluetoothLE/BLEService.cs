﻿using System;

using Xamarin.Forms;

namespace HatRemote.Networking.BluetoothLE
{
    public static class BLEService
    {
        public static Guid Guid => new Guid("5fd1c005-492c-4f33-87d3-a233ccf3c6e0");

        public static BLECharacteristics Characteristics => new BLECharacteristics();
    }
}

