﻿using System;
using System.Threading.Tasks;
using HatRemote.Models;
using Xamarin.Forms;

namespace HatRemote.Services
{
    public interface ILEDMatrixService
    {
        // Write (Action).
        Task ClearAsync();
        Task FlipHAsync();
        Task FlipVAsync();
        Task RotateAsync(int angle);

        // Write (Action).
        Task ShowMessageAsync(string message);

        // Read, Write.
        Task SetPixelsAsync(LEDMatrix ledMatrix);
        void GetPixels(Action<LEDMatrix> callback);

        // Read, Write
        Task SetPixelAsync(LEDCell ledCell);

        // Read.
        Task<Capabilities> GetCapabilities();
    }
}