﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using HatRemote.Models;
using HatRemote.Networking.Actions;
using HatRemote.Services;

namespace HatRemote.Networking
{
    public class HardwareService : ILEDMatrixService, IEnvironmentalSensorsService, IIMUSensorsService
    {
        private readonly ICapabilitiesCommand capabilities;
        private readonly IEnvironmentalSensorsCommand environmentSensors;
        private readonly IIMUSensorsCommand imuSensors;
        private readonly ILEDCommand led;
        private readonly IPixelsCommand pixels;
        private readonly IPixelCommand pixel;

        private HardwareService(
            ICapabilitiesCommand capabilities,
            IEnvironmentalSensorsCommand environmentSensors,
            IIMUSensorsCommand imuSensors,
            ILEDCommand led,
            IPixelsCommand pixels,
            IPixelCommand pixel)
        {
            this.capabilities = capabilities;
            this.environmentSensors = environmentSensors;
            this.imuSensors = imuSensors;
            this.led = led;
            this.pixels = pixels;
            this.pixel = pixel;
        }

        #region IIMUSensorsService
        void IIMUSensorsService.Read(Action<IMUSensors> callback)
        {
            imuSensors.Read(callback);
        }
        #endregion

        #region IEnvironmentalSensorsService
        void IEnvironmentalSensorsService.Read(Action<EnvironmentalSensors> callback)
        {
            environmentSensors.Read(callback);
        }
        #endregion

        #region ILEDMatrixService
        public async Task ClearAsync() => await led.WriteAsync(LEDCommandArg.Clear());

        public async Task FlipHAsync() => await led.WriteAsync(LEDCommandArg.FlipH());

        public async Task FlipVAsync() => await led.WriteAsync(LEDCommandArg.FlipV());

        public void GetPixels(Action<LEDMatrix> callback) => pixels.Read(callback);

        public async Task<Capabilities> GetCapabilities() => await capabilities.ReadAsync();

        public async Task RotateAsync(int angle) => await led.WriteAsync(LEDCommandArg.Rotate(angle));

        public async Task SetPixelAsync(LEDCell ledCell) => await pixel.WriteAsync(ledCell);

        public async Task SetPixelsAsync(LEDMatrix ledMatrix) => await pixels.WriteAsync(ledMatrix);

        public async Task ShowMessageAsync(string message) => await led.WriteAsync(LEDCommandArg.Message(message));
        #endregion

        #region Factory Method(s)

        static HardwareService()
        {
            _config.PropertyChanged += (object sender, PropertyChangedEventArgs e) =>
            {
                _instance = null;
            };
        }

        private static Configuration _config => Configuration.Instance;
        private static Lazy<HardwareService> _instance = new Lazy<HardwareService>(ForBLE);
        public static HardwareService Instance = _instance.Value;
        
        private static HardwareService ForBLE()
        {
            return new HardwareService(
                new BluetoothLE.Actions.CapabilitiesCommand(BluetoothLE.BLEManager.ConfiguredDevice),
                new BluetoothLE.Actions.EnviromentalSensorsCommand(BluetoothLE.BLEManager.ConfiguredDevice),
                new BluetoothLE.Actions.IMUSensorsCommand(BluetoothLE.BLEManager.ConfiguredDevice),
                new BluetoothLE.Actions.LEDCommand(BluetoothLE.BLEManager.ConfiguredDevice),
                new BluetoothLE.Actions.PixelsCommand(BluetoothLE.BLEManager.ConfiguredDevice),
                new BluetoothLE.Actions.PixelCommand(BluetoothLE.BLEManager.ConfiguredDevice));
        }

        #endregion
    }
}
