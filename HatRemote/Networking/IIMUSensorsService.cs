﻿using System;
using HatRemote.Models;

namespace HatRemote.Services
{
    public interface IIMUSensorsService
    {
        // Read.
        void Read(Action<IMUSensors> callback);
    }
}