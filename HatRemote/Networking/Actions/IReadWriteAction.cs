﻿namespace HatRemote.Networking.Actions
{
    public interface IReadWriteAction<TIn, TOut> : IReadAction<TOut>, IWriteAction<TIn>
    {
    }
}


