﻿using HatRemote.Models;

namespace HatRemote.Networking.Actions
{
    public interface ICapabilitiesCommand : IReadAsyncAction<Capabilities>
    {
    }
}