﻿using HatRemote.Models;

namespace HatRemote.Networking.Actions
{
    public interface IPixelCommand : IWriteAction<LEDCell>
    {
    }
}