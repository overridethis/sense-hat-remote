﻿using HatRemote.Models;

namespace HatRemote.Networking.Actions
{
    public interface IIMUSensorsCommand : IReadAction<IMUSensors>
    {
    }
}