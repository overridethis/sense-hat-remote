﻿using HatRemote.Models;

namespace HatRemote.Networking.Actions
{
    public interface IPixelsCommand : IReadWriteAction<LEDMatrix, LEDMatrix>
    {
    }
}