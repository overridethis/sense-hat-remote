﻿using System;

namespace HatRemote.Networking.Actions
{
    public interface IReadAction<TOut>
    {
        void Read(Action<TOut> callback);
    }
}
