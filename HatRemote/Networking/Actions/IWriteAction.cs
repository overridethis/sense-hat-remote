﻿using System.Threading.Tasks;

namespace HatRemote.Networking.Actions
{
    public interface IWriteAction<TIn> 
    {
        Task WriteAsync(TIn data);
    }
}


