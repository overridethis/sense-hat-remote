﻿using HatRemote.Models;

namespace HatRemote.Networking.Actions
{
    public interface IEnvironmentalSensorsCommand : IReadAction<EnvironmentalSensors>
    {
    }
}