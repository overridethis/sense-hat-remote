﻿using HatRemote.Models;

namespace HatRemote.Networking.Actions
{
    public interface ILEDCommand : IWriteAction<LEDCommandArg>
    {
    }
}