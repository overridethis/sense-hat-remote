﻿using System.Threading.Tasks;

namespace HatRemote.Networking.Actions
{
    public interface IReadAsyncAction<TOut>
    {
        Task<TOut> ReadAsync();
    }
}
