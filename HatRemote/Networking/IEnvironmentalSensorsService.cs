﻿using System;
using System.Threading.Tasks;
using HatRemote.Models;

namespace HatRemote.Services
{
    public interface IEnvironmentalSensorsService
    {
        // Read.
        void Read(Action<EnvironmentalSensors> callback);
    }
}