﻿using System.ComponentModel;
using System.Threading.Tasks;
using HatRemote.Models;
using HatRemote.Services;
using Xamarin.Forms;

namespace HatRemote.ViewModels
{
    public class PixelsViewModel : INotifyPropertyChanged
    {
        private readonly ILEDMatrixService _service;

        public PixelsViewModel(
            ILEDMatrixService service)
        {
            _service = service;
            _matrix = LEDMatrix.Empty();
        }

        public void Initialize()
        {
            _service.GetPixels(matrix => 
            {
                Matrix = matrix;
            });
        }

        private LEDMatrix _matrix;
        public LEDMatrix Matrix 
        {
            get 
            {
                return _matrix;
            } 
            private set
            {
                _matrix = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Matrix)));
            }
        }

        private int _r;
        public int R 
        {
            get 
            {
                return _r;
            }
            set
            {
                _r = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(R)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Color)));
            }
        }

        private int _g;
        public int G
        {
            get
            {
                return _g;
            }
            set
            {
                _g = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(G)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Color)));
            }
        }


        private int _b;
        public int B
        {
            get
            {
                return _b;
            }
            set
            {
                _b = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(B)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Color)));
            }
        }

        public Color Color
        {
            get
            {
                return Color.FromRgb(R, G, B);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
