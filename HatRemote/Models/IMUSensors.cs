﻿using System.ComponentModel;

namespace HatRemote.Models
{
    public class IMUSensors : INotifyPropertyChanged
    {
        private IMUSensor _orientation;
        private IMUSensor _gyroscope;
        private IMUSensor _accelerometer;
        private double _compass;

        public IMUSensor Orientation
        {
            get => _orientation; 
            set
            {
                _orientation = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Orientation)));
            }
        }

        public IMUSensor Gyroscope 
        { 
            get => _gyroscope; set
            {
                _gyroscope = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Gyroscope)));
            }
        }
        public IMUSensor Accelerometer 
        { 
            get => _accelerometer; set
            {
                _accelerometer = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Accelerometer)));
            }
        }
        public double Compass
        {
            get => _compass;
            set
            {
                _compass = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Compass)));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
