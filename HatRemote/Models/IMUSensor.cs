﻿using System;
using System.ComponentModel;

namespace HatRemote.Models
{

    public class IMUSensor : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private double _pitch;
        public double Pitch
        {
            get { return _pitch; }
            set
            {
                _pitch = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Pitch)));
            }
        }

        private double _roll;
        public double Roll
        {
            get { return _roll; }
            set
            {
                _roll = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Roll)));
            }
        }

        private double _yaw;
        public double Yaw
        {
            get { return _yaw; }
            set
            {
                _yaw = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Yaw)));
            }
        }
    }
}
