﻿using System;
using System.ComponentModel;

namespace HatRemote.Models
{
    public class EnvironmentalSensors : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private double _temperature;
        public double Temperature
        {
            get { return _temperature; }
            set
            {
                _temperature = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Temperature)));
            }
        }

        private double _pressure;
        public double Pressure
        {
            get { return _pressure; }
            set
            {
                _pressure = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Pressure)));
            }
        }

        private double _humidity;
        public double Humidity
        {
            get { return _humidity; }
            set
            {
                _humidity = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Humidity)));
            }
        }
    }
}
