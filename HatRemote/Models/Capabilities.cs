﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json.Linq;

namespace HatRemote.Models
{
    public class Capabilities : INotifyPropertyChanged
    {
        private ENVSensorsSupport environment = new ENVSensorsSupport();
        private LEDMatrixSupport ledMatrix = new LEDMatrixSupport();
        private IMUSensorsSupport imuSensors = new IMUSensorsSupport();

        public event PropertyChangedEventHandler PropertyChanged;

        public ENVSensorsSupport ENVSensors
        {
            get => environment;
            private set
            {
                environment = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ENVSensors)));
            }
        }

        public LEDMatrixSupport LEDMatrix
        {
            get => ledMatrix;
            private set
            {
                ledMatrix = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(LEDMatrix)));
            }
        }

        public IMUSensorsSupport IMUSensors
        {
            get => imuSensors;
            private set
            {
                imuSensors = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IMUSensors)));
            }
        }

        public void Update(Capabilities other)
        {
            ENVSensors.IsSupported = other.ENVSensors.IsSupported;
            ENVSensors.Temperature = other.ENVSensors.IsSupported && other.ENVSensors.Temperature;
            ENVSensors.Humidity = other.ENVSensors.IsSupported && other.ENVSensors.Humidity;
            ENVSensors.Pressure = other.ENVSensors.IsSupported && other.ENVSensors.Pressure;


            LEDMatrix.IsSupported = other.LEDMatrix.IsSupported;
            LEDMatrix.Height = LEDMatrix.IsSupported ? other.LEDMatrix.Height : 0;
            LEDMatrix.Width = LEDMatrix.IsSupported ? other.LEDMatrix.Width : 0; 

            IMUSensors.IsSupported = other.IMUSensors.IsSupported;
            IMUSensors.Orientation = other.IMUSensors.IsSupported && other.IMUSensors.Orientation;
            IMUSensors.Gyroscope = other.IMUSensors.IsSupported && other.IMUSensors.Gyroscope;
            IMUSensors.Accelerometer = other.IMUSensors.IsSupported && other.IMUSensors.Accelerometer;
            IMUSensors.Compass = other.IMUSensors.IsSupported && other.IMUSensors.Compass;
            
        }

        public static Capabilities Parse(JObject j)
        {
            var cap = new Capabilities();

            cap.ENVSensors.IsSupported = j["env"].Value<bool>();
            if (cap.ENVSensors.IsSupported)
            {
                cap.ENVSensors.Temperature = j["env_options"]["temperature"].Value<bool>();
                cap.ENVSensors.Humidity = j["env_options"]["humidity"].Value<bool>();
                cap.ENVSensors.Pressure = j["env_options"]["pressure"].Value<bool>();
            }

            cap.IMUSensors.IsSupported = j["imu"].Value<bool>();
            if (cap.IMUSensors.IsSupported)
            {
                cap.IMUSensors.Orientation = j["imu_options"]["orientation"].Value<bool>();
                cap.IMUSensors.Gyroscope = j["imu_options"]["gyroscope"].Value<bool>();
                cap.IMUSensors.Accelerometer = j["imu_options"]["accelerometer"].Value<bool>();
                cap.IMUSensors.Compass = j["imu_options"]["compass"].Value<bool>();
            }

            cap.ENVSensors.IsSupported = j["env"].Value<bool>();
            if (cap.ENVSensors.IsSupported)
            {
                cap.ENVSensors.Temperature = j["env_options"]["temperature"].Value<bool>();
                cap.ENVSensors.Humidity = j["env_options"]["humidity"].Value<bool>();
                cap.ENVSensors.Pressure = j["env_options"]["pressure"].Value<bool>();
            }

            cap.LEDMatrix.IsSupported = j["led"].Value<bool>();
            if (cap.LEDMatrix.IsSupported)
            {
                cap.LEDMatrix.Height = j["led_options"]["width"].Value<int>();
                cap.LEDMatrix.Width = j["led_options"]["height"].Value<int>();
            }

            return cap;
        }

    }

    public class ENVSensorsSupport : INotifyPropertyChanged
    {
        private bool isSupported;
        private bool temperature;
        private bool humidity;
        private bool pressure;

        public bool IsSupported
        {
            get => isSupported;
            set
            {
                isSupported = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsSupported)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            }
        }

        public bool Temperature
        {
            get => temperature;
            set
            {
                temperature = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Temperature)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            }
        }

        public bool Humidity
        {
            get => humidity;
            set
            {
                humidity = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Humidity)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            }
        }

        public bool Pressure
        {
            get => pressure;
            set
            {
                pressure = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Pressure)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            }
        }
        
        public string Description
        {
            get
            {
                if (!IsSupported)
                    return "Not Supported";

                var features = new List<string>();
                if (Temperature) features.Add(nameof(Temperature));
                if (Humidity) features.Add(nameof(Humidity));
                if (Pressure) features.Add(nameof(Pressure));

                return string.Join(", ", features);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class LEDMatrixSupport : INotifyPropertyChanged
    {
        private int width;
        private int height;
        private bool isSupported;

        public bool IsSupported
        {
            get => isSupported;
            set
            {
                isSupported = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsSupported)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            }
        }

        public int Height
        {
            get => height;
            set
            {
                height = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Height)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            }
        }

        public int Width
        {
            get => width;
            set
            {
                width = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Width)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            }
        }

        public string Description => IsSupported ? $"{Width} x {Height} RGB LED Display" : "Not Supported";

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class IMUSensorsSupport : INotifyPropertyChanged
    {
        private bool isSupported;
        private bool orientation;
        private bool gyroscope;
        private bool accelerometer;
        private bool compass;

        public bool IsSupported
        {
            get => isSupported;
            set
            {
                isSupported = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsSupported)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            }
        }

        public bool Orientation
        {
            get => orientation;
            set
            {
                orientation = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Orientation)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            }
        }

        public bool Gyroscope
        {
            get => gyroscope;
            set
            {
                gyroscope = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Gyroscope)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            }
        }

        public bool Accelerometer
        {
            get => accelerometer;
            set
            {
                accelerometer = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Accelerometer)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            }
        }

        public bool Compass
        {
            get => compass;
            set
            {
                compass = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Compass)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            }
        }

        public string Description
        {
            get
            {
                if (!IsSupported)
                    return "Not Supported";

                var features = new List<string>();
                if (Orientation) features.Add(nameof(Orientation));
                if (Gyroscope) features.Add(nameof(Gyroscope));
                if (Accelerometer) features.Add(nameof(Accelerometer));
                if (Compass) features.Add(nameof(Compass));

                return string.Join(", ", features);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
