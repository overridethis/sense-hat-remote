﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;

namespace HatRemote.Models
{
    public class LEDMatrix
    {
        public LEDMatrix() : this(new ObservableCollection<LEDCell>())
        {
        }
        public LEDMatrix(ObservableCollection<LEDCell> cells)
        {
            Cells = cells;
        }

        public ObservableCollection<LEDCell> Cells { get; }

        public static LEDMatrix From(IList<int[]> raw)
        {
            return new LEDMatrix(
                new ObservableCollection<LEDCell>(
                    raw.Select((r,i) => new LEDCell { Index = i, Color = Color.FromRgb(r[0], r[1], r[2]) }
                )));
        }

        public static LEDMatrix Empty()
        {
            var cells = new ObservableCollection<LEDCell>();
            for (var i = 0; i < 64; i++)
            {
                cells.Add(
                    new LEDCell
                    {
                        Index = i,
                        Color = Color.FromRgb(0, 0, 0),
                    });
            }
            return new LEDMatrix(cells);
        }
    }
}
