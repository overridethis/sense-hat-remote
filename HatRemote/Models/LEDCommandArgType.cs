﻿using System;
namespace HatRemote.Models
{
    public enum LEDCommandType
    {
        Clear,
        FlipH,
        FlipV,
        Message,
        Rotate,
    }

}
