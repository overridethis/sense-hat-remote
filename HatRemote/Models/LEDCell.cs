﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;

namespace HatRemote.Models
{
    public class LEDCell : INotifyPropertyChanged
    {
        public int Index { get; set; }

        private Color _color;
        public Color Color
        {
            get
            {
                return _color;
            }

            set
            {
                _color = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Color)));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Point Coordinate => IndexToCoord(this.Index);

        public object ToPayload()
        {
            var coord = IndexToCoord(this.Index);
            var req = new
            {
                x = Convert.ToInt32(coord.X),
                y = Convert.ToInt32(coord.Y),
                r = Convert.ToInt32(this.Color.R * 255),
                g = Convert.ToInt32(this.Color.G * 255),
                b = Convert.ToInt32(this.Color.B * 255),
            };
            return req;
        }

        public static Point IndexToCoord(int index)
        {
            var x = 0d;
            var y = 0d;
            if (index == 0)
                return new Point(0, 0);

            x = index % 8;
            y = index / 8;

            return new Point(x, y);
        }

    }
}
