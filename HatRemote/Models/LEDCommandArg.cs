﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace HatRemote.Models
{
    public class LEDCommandArg
    {
        public LEDCommandArg(
            LEDCommandType commandType)
        {
            CommandType = commandType;
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public LEDCommandType CommandType { get; }

        public static LEDCommandArg Clear() => new LEDCommandArg(LEDCommandType.Clear);
        public static LEDCommandArg FlipH() => new LEDCommandArg(LEDCommandType.FlipH);
        public static LEDCommandArg FlipV() => new LEDCommandArg(LEDCommandType.FlipV);
        public static LEDCommandArg<int> Rotate(int angle) => new LEDCommandArg<int>(LEDCommandType.Rotate, angle);
        public static LEDCommandArg<string> Message(string message) => new LEDCommandArg<string>(LEDCommandType.Message, message);
    }

    public class LEDCommandArg<TArgs> : LEDCommandArg
    {
        public LEDCommandArg(
            LEDCommandType commandType,
            TArgs args) : base(commandType)
        {
            Args = args;
        }

        public TArgs Args { get; }

    }
}
