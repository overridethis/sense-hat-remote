﻿using System.ComponentModel;
using HatRemote.Models;
using Newtonsoft.Json;

namespace HatRemote.Services
{
    public class Configuration : IConfiguration
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private static readonly string DEVICENAME_KEY = "DEVICENAME_KEY";
        private static readonly string CAPABILITIES_KEY = "CAPABILITIES_KEY";

        private Configuration()
        {
            this.capabilities = new Capabilities();
            this.capabilities.PropertyChanged += ChildrenPropertyChanged;
        }

        private static Configuration _instance;
        public static Configuration Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Configuration();
                    _instance.LoadFromCache();
                }

                return _instance;
            }
        }

        private string deviceName;
        private Capabilities capabilities = new Capabilities();

        public string DeviceName
        {
            get
            {
                return deviceName;
            }
            set
            {
                deviceName = value;
                Xamarin.Forms.Application.Current.Properties[DEVICENAME_KEY] = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(DeviceName)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Capabilities)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsConfigured)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsSenseHat)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            }
        }


        public Capabilities Capabilities
        {
            get
            {
                return capabilities;
            }
            set
            {
                capabilities = value;
                capabilities.PropertyChanged += ChildrenPropertyChanged;
                Xamarin.Forms.Application.Current.Properties[CAPABILITIES_KEY] = JsonConvert.SerializeObject(value);
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(DeviceName)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Capabilities)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsConfigured)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsSenseHat)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            }
        }

        private void LoadFromCache()
        {
            if (Xamarin.Forms.Application.Current != null)
            {
                if (Xamarin.Forms.Application.Current.Properties.ContainsKey(DEVICENAME_KEY))
                {
                    deviceName = (string)Xamarin.Forms.Application.Current.Properties[DEVICENAME_KEY];
                }

                if (Xamarin.Forms.Application.Current.Properties.ContainsKey(CAPABILITIES_KEY))
                {
                    if (capabilities != null)
                    {
                        capabilities.PropertyChanged -= ChildrenPropertyChanged;
                    }

                    var serialized = (string)Xamarin.Forms.Application.Current.Properties[CAPABILITIES_KEY];
                    capabilities = JsonConvert.DeserializeObject<Capabilities>(serialized);
                    capabilities.PropertyChanged += ChildrenPropertyChanged;
                }
            }
        }

        private void ChildrenPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(DeviceName)));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Capabilities)));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsConfigured)));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Description)));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsSenseHat)));
        }

        public bool IsConfigured => !string.IsNullOrWhiteSpace(DeviceName);

        public string Description
        {
            get
            {
                return IsConfigured
                    ? $"Connected: {DeviceName}"
                    : $"Not Configured.";
            }
        }

        public bool IsSenseHat
        {
            get
            {
                return Capabilities.LEDMatrix.Height == 8;
            }
        }
    }
}
