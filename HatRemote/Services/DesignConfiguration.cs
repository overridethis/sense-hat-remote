﻿using System;
using System.ComponentModel;
using HatRemote.Models;

namespace HatRemote.Services
{
    public class DesignConfiguration : IConfiguration
    {
        public string DeviceName { get; }

        public bool IsConfigured => true;

        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsSenseHat => true;

        public string Description => "Configured";

        public Capabilities Capabilities { get; set; } = new Capabilities();
    }
}
