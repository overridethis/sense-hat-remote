﻿using System;
using System.ComponentModel;
using HatRemote.Models;

namespace HatRemote.Services
{
    public interface IConfiguration : INotifyPropertyChanged
    {
        string DeviceName { get; }
        bool IsConfigured { get; }
        string Description { get; }
        bool IsSenseHat { get; }
        Capabilities Capabilities { get; }
    }
}
