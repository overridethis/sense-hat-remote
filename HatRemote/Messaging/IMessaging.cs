﻿using System;
using Xamarin.Forms;

namespace HatRemote.Messaging
{
    public interface IMessaging
    {
        void Subscribe<TSender, TArgs>(
            object subscriber,
            string message,
            Action<TSender, TArgs> callback,
            TSender source = null) where TSender : class;

        void Unsubscribe<TSender, TArgs>(
            object subscriber,
            string message) where TSender : class;

        void Send<TSender, TArgs>(
            TSender sender, 
            string message, 
            TArgs args) where TSender : class;
    }

    public class Messaging : IMessaging
    {
        public void Send<TSender, TArgs>(
            TSender sender, 
            string message,
            TArgs args) where TSender: class => MessagingCenter.Instance.Send(sender, message, args);

        public void Subscribe<TSender, TArgs>(
            object subscriber, 
            string message,
            Action<TSender, TArgs> callback,
            TSender source = null) where TSender : class => MessagingCenter.Instance.Subscribe(subscriber, message, callback, source);

        public void Unsubscribe<TSender, TArgs>(
            object subscriber,
            string message) where TSender : class => MessagingCenter.Instance.Unsubscribe<TSender, TArgs>(subscriber, message);
    }
}
