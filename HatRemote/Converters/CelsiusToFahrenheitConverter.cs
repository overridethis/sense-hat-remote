﻿using System;
using System.ComponentModel;
using System.Globalization;
using Xamarin.Forms;

namespace HatRemote.Converters
{
    public class CelsiusToFahrenheitConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var c = (double)value;

            return (c * 9 / 5) + 32;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
