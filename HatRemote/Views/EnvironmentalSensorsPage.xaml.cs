﻿using System;
using System.Collections.Generic;
using HatRemote.Services;
using HatRemote.Networking;
using Xamarin.Forms;

namespace HatRemote.Views
{
    public partial class EnvironmentalSensorsPage : ContentPage
    {
        private readonly IEnvironmentalSensorsService service;

        public EnvironmentalSensorsPage() : this(HardwareService.Instance)
        {
        }
        public EnvironmentalSensorsPage(IEnvironmentalSensorsService service)
        {
            this.service = service;
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            service.Read(env =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    this.BindingContext = env;
                });
            });
        }
    }
}
