﻿using System;
using System.Collections.Generic;
using HatRemote.Services;
using HatRemote.Networking;
using Xamarin.Forms;

namespace HatRemote.Views
{
    public partial class IMUSensorsPage : ContentPage
    {
        private readonly IIMUSensorsService service;

        public IMUSensorsPage() : this(HardwareService.Instance)
        {
        }
        public IMUSensorsPage(IIMUSensorsService service)
        {
            this.service = service;
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            service.Read(env =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    this.BindingContext = env;
                });
            });
        }
    }
}
