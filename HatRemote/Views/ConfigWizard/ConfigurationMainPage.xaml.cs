﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HatRemote.Views.ConfigWizard
{
    public partial class ConfigurationMainPage : ContentPage
    {
        public void Item_Tapped(object sender, System.EventArgs e)
        {
            var btn = (Button)sender;
            this.Navigation.PushAsync(new ConfigurationBluetoothDiscover());
        }

        public ConfigurationMainPage()
        {
            InitializeComponent();
        }
    }
}
