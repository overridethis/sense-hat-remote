﻿using System.Collections.ObjectModel;
using HatRemote.Networking;
using HatRemote.Networking.BluetoothLE;
using HatRemote.Services;
using Xamarin.Forms;

namespace HatRemote.Views.ConfigWizard
{
    public partial class ConfigurationBluetoothDiscover : ContentPage
    {
        private readonly Configuration config;
        private readonly HardwareService hardware;

        public ConfigurationBluetoothDiscover() : this(
            Configuration.Instance,
            HardwareService.Instance)
        {
        }
        public ConfigurationBluetoothDiscover(
            Configuration config,
            HardwareService hardware)
        {
            InitializeComponent();
            this.config = config;
            this.hardware = hardware;
            this.BindingContext = new ObservableCollection<string>();
        }

        public ObservableCollection<string> Devices 
        {
            get
            {
                return this.BindingContext as ObservableCollection<string>;   
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.Devices.Clear();

            BLEManager.Scan(peripheral =>
            {
                this.Devices.Add(peripheral);
            });
        }

        public async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var deviceName = (string)e.Item;
            config.DeviceName = deviceName;
            config.Capabilities.Update(await hardware.GetCapabilities());

            // HACK: Does wonder for the Binding on MainPage
            config.DeviceName = deviceName;
            
            await this.DisplayAlert(
                "Configuration",
                $"Connection complete ({config.DeviceName}).", "Ok");
            await this.Navigation.PopToRootAsync(true);
        }
    }
}
