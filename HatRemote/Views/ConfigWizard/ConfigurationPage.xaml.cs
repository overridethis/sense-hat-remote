﻿using HatRemote.Services;
using Xamarin.Forms;

namespace HatRemote.Views.ConfigWizard
{
    public partial class ConfigurationPage : ContentPage
    {
        public Configuration Model { get { return this.BindingContext as Configuration; } }

        public ConfigurationPage() :this(Configuration.Instance)
        {
        }
        public ConfigurationPage(Configuration config) 
        {
            InitializeComponent();
            BindingContext = config;
        }

        private async void Configure_Clicked(object sender, System.EventArgs e)
        {
            // Validate Configuration.
            this.Model.ConfigType = ConfigType.HTTP;

            await this.DisplayAlert(
                "Configuration",
                $"Connection complete ({this.Model.Description}).", "Ok");
            await this.Navigation.PopToRootAsync();
        }
    }
}
