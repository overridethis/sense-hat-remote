﻿using HatRemote.Services;
using HatRemote.Networking;
using Xamarin.Forms;

namespace HatRemote.Views
{
    public partial class LEDMatrixPage : ContentPage
    {
        private readonly ILEDMatrixService _led;

        public LEDMatrixPage() 
            : this(HardwareService.Instance)
        {
        }
        public LEDMatrixPage(
            ILEDMatrixService led)
        {
            InitializeComponent();
            _led = led;
        }

        private async void Generic_Tapped(object sender, System.EventArgs e)
        {
            var action = (TextCell)sender;
            // Rotate.
            if (action.ClassId.StartsWith("Rotate"))
            {
                var angle = int.Parse(action.ClassId.Replace("Rotate", ""));
                await _led.RotateAsync(angle);
            }

            // Clean.
            switch(action.ClassId)
            {
                case "Clear":
                    await _led.ClearAsync();
                    break;
                case "FlipH":
                    await _led.FlipHAsync();
                    break;
                case "FlipV":
                    await _led.FlipVAsync();
                    break;
                case "Pixels":
                    await Navigation.PushAsync(new PixelsPage());
                    break;
            }
        }

        private async void ShowMessage_Tapped(object sender, System.EventArgs e)
        {
            var msg = this.MessageEntryCell.Text;
            await _led.ShowMessageAsync(msg);
            this.MessageEntryCell.Text = string.Empty;
        }
    }
}
