﻿using HatRemote.Models;
using HatRemote.Services;
using Xamarin.Forms;

namespace HatRemote.Views
{
    public partial class MainPage : ContentPage
    {
        private IConfiguration Config
        {
            get
            {
                return this.BindingContext as IConfiguration;
            }
        }

        public MainPage(IConfiguration config)
        {
            this.BindingContext = config;
        }
        public MainPage() : this(Configuration.Instance)
        {
            InitializeComponent();
        }

        private void Item_Tapped(object sender, System.EventArgs e)
        {
            var cell = (TextCell)sender;
            switch (cell.ClassId)
            {   
                case "Configuration":
                    this.Navigation.PushAsync(new ConfigWizard.ConfigurationMainPage());
                    break;
                case "EnvironmentalSensors":
                    this.Navigation.PushAsync(new EnvironmentalSensorsPage());
                    break;
                case "IMUSensor":
                    this.Navigation.PushAsync(new IMUSensorsPage());
                    break;
                case "LEDMatrix":
                default:
                    this.Navigation.PushAsync(new LEDMatrixPage());
                    break;
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (!Config.IsConfigured)
            {
                this.Navigation.PushAsync(new ConfigWizard.ConfigurationMainPage());
            }
        }

        private void ToolbarItemActivated(object sender, System.EventArgs e)
        {
            this.Navigation.PushAsync(new ConfigWizard.ConfigurationMainPage());
        }
    }
}
