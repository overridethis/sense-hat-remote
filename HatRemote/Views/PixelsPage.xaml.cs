﻿using HatRemote.Models;
using HatRemote.Services;
using HatRemote.Networking;
using HatRemote.ViewModels;
using Xamarin.Forms;

namespace HatRemote.Views
{
    public partial class PixelsPage : ContentPage
    {
        private readonly ILEDMatrixService _led;

        public PixelsViewModel ViewModel { get { return BindingContext as PixelsViewModel; }}

        public PixelsPage() : this(HardwareService.Instance)
        {
        }
        public PixelsPage(ILEDMatrixService led)
        {
            InitializeComponent();
            _led = led;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            var vm = new PixelsViewModel(_led);
            InitializeGrid(8, 8);

            BindingContext = vm;
            vm.Initialize();
        }

        private void InitializeGrid(int width, int height)
        {
            // Add row.
            for (var row = 0; row < height; row++)
                this.LEDPanel.RowDefinitions.Add(new RowDefinition());

            // Add columns.
            for (var column = 0; column < width; column++)
                this.LEDPanel.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            // Add buttons.
            for(var row = 0; row < height; row++)
            {
                for (var column = 0; column < width; column++)
                {
                    var button = new Button();
                    button.SetBinding(Button.BindingContextProperty, $"Matrix.Cells[{(row * width) + column}])");
                    button.SetBinding(Button.BackgroundColorProperty, "Color");
                    button.ClassId = column.ToString();
                    button.Clicked += HandlePixelClicked;

                    button.SetValue(Grid.ColumnProperty, column);
                    button.SetValue(Grid.RowProperty, row);

                    this.LEDPanel.Children.Add(button);
                }
            }
        }

        private async void HandlePixelClicked(object sender, System.EventArgs e)
        {
            var btn = (Button)sender;
            var model = (LEDCell)btn.BindingContext;
            model.Color = ViewModel.Color;
            await _led.SetPixelAsync(model);
        }
    }
}
