﻿using HatRemote.Services;
using HatRemote.Views;
using Xamarin.Forms;

namespace HatRemote
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            var main = new MainPage();
            var nav = new NavigationPage(main);
            MainPage = nav;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
